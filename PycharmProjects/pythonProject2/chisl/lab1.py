import math


def f(x):
    #return 1-math.pow(math.e,x)
    return math.sin(x)/x


a = -1
b = 5
epsilon = 1e-3

while abs(a-b)>epsilon:
    c = (a+b)/2
    #print(c)
    if f(a)*f(c)>0:
        a = c
    else:
        b = c

print((a+b)/2)  # приблизительный корень уравнения f(x)=0
