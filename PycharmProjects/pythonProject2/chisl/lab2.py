import numpy as np
def Gauss(m):
    # избавление от столбцов
    for col in range(len(m[0])):
        for row in range(col+1, len(m)):
            r = [(rowValue * (-(m[row][col] / m[col][col]))) for rowValue in m[col]]
            m[row] = [sum(pair) for pair in zip(m[row], r)]
    # обратное решение путем замены

    ans = []
    m.reverse()
    for sol in range(len(m)):
            if sol == 0:
                ans.append(m[sol][-1] / m[sol][-2])
            else:
                inner = 0
                # substitute in all known coefficients
                for x in range(sol):
                    inner += (ans[x]*m[sol][-2-x])
                # уравнение принимает вид ax + b = c
                # находим х = (c - b) / a
                ans.append((m[sol][-1]-inner)/m[sol][-sol-2])
    ans.reverse()
    return ans

Ab = [ [2,4,1,3],
       [1,6,7,5],
       [5,2,3,1] ]

A = [i[:-1] for i in Ab]
b = [i[-1:] for i in Ab]
A = np.asarray(A)
b = np.asarray(b)
print("A:\n"+str(A))
print("\nb:\n"+str(b))


x = np.asarray([Gauss(Ab)]).T
print("\nРешение x\n"+str(x))



# проверка
print("\nПроверка Ax-b~0: \n"+str(np.dot(A,x)-b))  # ноль или почти ноль

