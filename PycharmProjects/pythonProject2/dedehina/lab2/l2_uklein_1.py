# график 25
import math


def f(x):
    if x < -2:
        return 1
    elif x > 2:
        return -3
    else:
        return -math.sqrt(4-pow(x,2))


print(f(-100))
print(f(0))
print(f(50))
