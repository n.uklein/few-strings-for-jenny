# номер 25
def divs(n):  # возвращает массив всех собственных делителей числа
    i = 1
    arr = []
    for i in range(i,n):
        if n%i==0:
            arr.append(i)
    return arr

def sum(arr):  # вернёт сумму собственных делителей
    sum = 0
    for i in arr:
        sum+=i
    return sum

def isExcessive(n):  # возвращает 1 если число изб., 0 в противном случае
    if sum(divs(n))>n:
        return 1
    else:
        return 0


exNumbers = []  # пустой массив под избыточные числа

n = 0
while len(exNumbers) < 10:  # находим первые 10 изб. чисел
    if isExcessive(n):
        exNumbers.append(n)
    n+=1
print("Последовательность первых 10 избыточных чисел: "+str(exNumbers))

sum_ex = sum(exNumbers)  # считаем сумму элементов этой последовательности
print("Сумма её элементов равна "+str(sum_ex))

if isExcessive(sum_ex):  # проверяе эту сумму на избыточность
    print("Эта сумма тоже избыточное число")
