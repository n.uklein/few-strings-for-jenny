# номер 1
import random

N = int(input("Введите число N - длинну массива "))
#arr = []
arr = [random.randint(0,100) for i in range(0,N)]  # массив длинной
_max = max(arr)
print(arr)
print(str(_max) + " - максимальный элемент")

right_sum = 0
i = -1
while arr[i]!=_max:
    right_sum+=arr[i]
    i-=1
print("Сумма элементов после максимального " +str(right_sum))