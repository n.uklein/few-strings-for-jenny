class CM:
    def __init__(self, _x=0.):  # по умолнчанию проиниц. нулём
        self.ih = 2.54
        self.ft = 30.48
        self.yd = 91.44
        self.value = _x

    def convertTo(self, measure='inch'):  # default inch
        if measure == 'inch':
            print(str(self.value)+" см = "+str(float(self.value)/self.ih)+" дюйма")
            return float(self.value)/self.ih
        if measure == 'foot':
            print(str(self.value)+" см = "+str(float(self.value)/self.ft)+" фута")
            return float(self.value)/self.ft
        if measure == 'yard':
            print(str(self.value)+" см = "+str(float(self.value)/self.yd)+" ярда")
            return float(self.value)/self.yd

    def get(self):  # метод который печатает наше см
        print(self.value)
    value = 0       # храним наш публичный см


a = CM()
a.get()         # 0
a.value = 10
a.get()         # 10
a.convertTo('yard')

b = CM(100)
b.get()         # 50
b.convertTo('foot')

c = CM(2.6)
c.get()         # 2.6
c.convertTo()
