# вар. номер 3
import math
import random


def z1(x):
    if math.cos(x)+math.cos(4*x) != 0:
        return (math.sin(2*x)+math.sin(5*x)-math.sin(3*x))/(math.cos(x)+math.cos(4*x))
    else:
        return "Деление на ноль"


def z2(x):
    return 2*math.sin(x)


# инициализируем последовательность случайных чисел от -4 до 4 размером 5
seq = [8*random.random()-4 for i in range(0, 5)]

# выполняем вычисления разными циклами языка
print("цикл по всем элементам массива")
for e in seq:
    print("alpha="+str(e)+"; z1="+str(z1(e))+"; z2="+str(z2(e)))
print("цикл по счетчику i")
for i in range(len(seq)):
    e = seq[i]
    print("alpha="+str(e)+"; z1="+str(z1(e))+"; z2="+str(z2(e)))
print("цикл while")
i = 0
while i < len(seq):
    e = seq[i]
    print("alpha="+str(e)+"; z1="+str(z1(e))+"; z2="+str(z2(e)))
    i += 1
