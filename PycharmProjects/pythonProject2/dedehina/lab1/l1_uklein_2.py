# номер 7
import math
import random


# определим функции
def y1(x): return math.pow(x,3)
def y2(x): return 1+math.pow(x,3)
def y3(x): return 1/(1+math.pow(x,2))


# y1
x0 = 100*random.random()-50  # инициализируем действительное значение х0 в промежутке (-50,50)
if y1(x0) == y1(-x0):
    print("y1 чётная")
elif y1(x0) == -y1(-x0):
    print("y1 нечетная")
else:
    print("y1 ни чётная, ни нечётная")

# y2
x0 = 100*random.random()-50  # инициализируем действительное значение х0 в промежутке (-50,50)
if y2(x0)==y2(-x0):
    print("y2 чётная")
elif y2(x0)==-y2(-x0):
    print("y2 нечетная")
else:
    print("y2 ни чётная, ни нечётная")

# y3
x0 = 100*random.random()-50  # инициализируем действительное значение х0 в промежутке (-50,50)
if y3(x0)==y3(-x0):
    print("y3 чётная")
elif y3(x0)==-y3(-x0):
    print("y3 нечетная")
else:
    print("y3 ни чётная, ни нечётная")
