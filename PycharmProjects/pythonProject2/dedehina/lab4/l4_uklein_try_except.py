# вариант 1
N = 5  # определяем длинну массива


def meanArray(n):  # инициализирует массив размера n с клавиатуры, возвращает среднее среди положительных
    arr = []
    for i in range(n):
        numb = int(input("Введите целое число "))
        arr.append(numb)
    count_positive = 0  # считаем количество и сумму положительных чисел
    sum_positive = 0
    for i in arr:
        if i > 0:
            count_positive += 1
            sum_positive += i
    return float(sum_positive)/count_positive


try:
    print(meanArray(N))
except ValueError as v_e:  # неверный тип (введена строка либо float)
    print(v_e)
except ZeroDivisionError as zd_e:  # ноль положительных целых чисел
    print(zd_e)
else:  # выполняется, если не было ошибок
    print("Ошибок нет")
finally:  # этот блок выполняется всегда
    print("Программа завершена")
