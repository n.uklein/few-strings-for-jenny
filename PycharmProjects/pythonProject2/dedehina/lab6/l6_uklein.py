s = "15 яблок. 1.5кг бананов. очень много груш! спелые арбузы."
print("Исходный текст\n" + s)


def func_upper(txt):
    txt = list(txt)
    k = 0
    while not txt[k].isalpha():  # преобразуем заглавную в самом первом предложении
        if txt[k].isdigit():  # это чтобы исключить варианты типа "15 марта" -> "15 Марта"
            break
        k += 1
    # print(k)
    txt[k] = txt[k].upper()

    for i in range(len(txt)):  # в последующих предложениях преобразуем заглавную только после разделителей
        if txt[i] in ['.', '!', '?']:  # когда "натыкаемся" на . ! ? преобразуем следующую букву
            for j in range(i, len(txt)):
                if txt[j].isdigit():  # аналогично как и в while
                    break
                if txt[j].isalpha():
                    # print(txt[j])
                    txt[j] = txt[j].upper()
                    break
    txt = "".join(txt)
    return txt


print("Исправленный текст")
print(func_upper(s))