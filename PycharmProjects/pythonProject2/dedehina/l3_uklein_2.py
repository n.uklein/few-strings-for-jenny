# номер 2
# пункт "а"
import random

#N = int(input("Введите размер матрицы (натуральное число) "))
N = 3
A = [[random.randint(-2,2) for j in range(0,N)] for i in range(0,N)]  # заполняем матрицу A(NxN) целыми числами
for i in A:  # выведем матрицу
    print(i)


def zero_el(arr):  # находит количество строк, содержащих 0, возвращает разницу N минус кол.во этих строк
    num = 0
    for i in arr:
        for e in i:
            if e == 0:
                num+=1
                break
    return N-num  # кол.во строк без единого нулевого элемента


print("a) Количество строк без 0 равно "+str(zero_el(A)))

# пункт "б"
arr = []  # массив под все элементы матрицы
for i in A:
    for j in i:
        arr.append(j)
arr_ = []  # массив под элементы, встречающиеся более 1 раза
for element in arr:
    if arr.count(element)>1:
        arr_.append(element)
#print(arr_)
max_element = max(arr_)
print("б) Максимальный из элементов, встречающихся более 1 раза в матрице, равен "+str(max_element))