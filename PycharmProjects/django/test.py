import configparser

config = configparser.ConfigParser()
config['settings'] = {
    'resolution': '320x240',
    'color': 'blue'
}

with open('example.ini','w') as configfile:
    config.write(configfile)

settings = config['settings']
settings['color'] = 'red'
