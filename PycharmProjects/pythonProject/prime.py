from datetime import datetime
import math as m

n = int(input(''))


def prime(n):
    if n==1 or n==2 or n==3:
        return "простое"
    elif n%2==0 or n%3==0:
        return "составное"

    else:
        e = m.sqrt(n)
        b = 5
        i = -1
        while b < e:
            if n%b==0:
                return "составное"
            b = b + 3 + i
            i = (-1)*i
        return "простое"


time1 = datetime.now()
print(prime(n))
time2 = datetime.now()
print(time2-time1)

