#переменная для хранения направлений direction,
#значения r,l,u,d (в кавычках)
#начальное направление - право
#текущее значение, которое вы уже поставили: curr_val=0

#17 16 15 14 13
#18  5 4  3  12
#19  6 1  2  11
#20  7 8  9  10
#21 22 23 24 25


# x x x x x
# x 5 4 3 x
# x 6 1 2 x
# x 7 8 x x
# x x x x x

def spiral(n):
    shape = 2*n + 1
    shape2 = shape**2
    arr = [[None for x in range(shape)] for y in range(shape)]
    direction = "r"
    curr_val = 0
    # координаты единицы
    i = n
    j = n
    while curr_val<shape2:
        curr_val += 1
        arr[i][j] = curr_val
        # при движении вправо координаты по левую руку
        # ячейки есть i-1, j
        if direction == "r":
            j = j+1
            if i>0 and arr[i-1][j]==None:
                direction = "u"
        if direction == "u":
            i = i-1
            if j > 0 and arr[i][j-1]==None:
                direction = "l"
        if direction == "l":
            j = j - 1
            if i < shape-1 and arr[i+1][j]==None:
                direction = "d"
        if direction == "d":
            i = i+1
            if j < shape-1 and arr[i][j+1]==None:
                direction = "r"


    return arr
x = spiral(3)
print(x)