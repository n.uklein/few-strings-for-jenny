def fig_sum(n): #сумма цифер числа
    sum = 0
    while n != 0:
        m = n % 10
        sum = sum + m
        n = (n - m) / 10
    return sum

def first(arr): #сумма всех делителей
    sum = 0
    for i in arr:
        sum += i
    return sum

def _divs(n): #выводит массив простых делителей
    mynum = n
    arr = []
    i = 1
    while n!=1:
        if n%i==0 and i!=1:
            n = n/i
            if i != mynum:
                arr.append(i)
            i = 1
        i=i+1
    return arr

def divs(n): #выводит моссив всех возможных делителей
    i = 1
    arr = []
    for i in range(i,n):
        if n%i==0:
            arr.append(i)
    return arr


for i in range(1,300000):
    if first(_divs(i))==i:
        print("Идеально по 1-у условию число: " + str(i) + "; делители: " +str(_divs(i)))
    if first(divs(i)) == i:
        print("Идеально по 3-у условию число: " + str(i) + "; делители: " + str(divs(i)))
    if fig_sum(i) == first(_divs(i)):
        print("Идеально по 2-у условию число: " + str(i) + "; делители: " + str(_divs(i)))

