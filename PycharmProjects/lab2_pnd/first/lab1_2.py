import math as m




def issimple(n):
    flag = 1
    if n == 1:
        return flag-1
    elif n == 2:
        return flag
    elif n == 3:
        return flag

    elif n % 2 == 0:
        return flag-1
    elif n % 3 == 0:
        return flag -1
    else:
        e = m.sqrt(n)
        b = 5
        i = 1
        while b < e:
            #print(b)
            if n%b==0:
                return flag - 1
            k = (-1) ** i
            a = 3 + k
            b = b + a
            i = i + 1
        return flag

def func(g):
    _arr = []
    k = 2
    for l in range(g-k, 3, -1):
        arr = []
        a,b,c,d = 0,0,0,0
        #print(k, l)
        i = 2
        m = 2
        for j in range(k - i, 1, -1):
            if issimple(i) == 1 and issimple(j) == 1:
                a = i
                b = j
                #print(str(k) + ' = ' + str(i) + " + " + str(j))
            i = i + 1
        for p in range(l - m, 1, -1):
            if issimple(m) == 1 and issimple(p) == 1:
                #print(str(l) + ' = ' + str(m) + " + " + str(p))
                c = m
                d = p
            m = m + 1
        if a!=0 and b!=0 and c!=0 and d!=0:
            #print(str(g) + ' = ' + str(a) + " + " + str(b) + " + " + str(c) + " + " + str(d))
            arr.append(a)
            arr.append(b)
            arr.append(c)
            arr.append(d)
            arr.sort()
            #print(arr)
            s = 0
            for e in _arr:
                if e == arr:
                    s = 1
            if s==0:
                _arr.append(arr)
        k = k + 1
    print(_arr)




numb = int(input())

func(numb)