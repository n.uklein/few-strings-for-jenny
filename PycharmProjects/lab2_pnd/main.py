import numpy as np
import pandas as pd
from scipy.stats import f
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from scipy.spatial.distance import mahalanobis


# install openpyxl

def get_train_data(data, features, train_samples=None):
    if train_samples:
        train_data = pd.DataFrame()
        for cls, samples in train_samples.items():
            train_samps = data[features].loc[samples]
            train_samps["Class"] = cls
            train_data = pd.concat([train_data, train_samps])
    else:
        assert data.shape[1] == len(features) + 1
        train_data = data.dropna()
        cls_col = train_data.drop(columns=features).columns[0]
        train_data = train_data.rename({cls_col: "Class"}, axis=1)
    train_data = train_data.astype({"Class": 'int32'})
    return train_data


def scatter_matrix(samples):
    # является ли подклассом?
    if isinstance(samples, pd.Series):
        samples = samples.to_frame()
    d = samples - samples.mean()
    res = np.zeros((d.shape[1], d.shape[1]))
    # приводит к виду int: 32, 24, ...
    for _, row in d.iterrows():
        col = row.to_frame()
        res += col @ col.T
    return res


def classes_scatter_matrix(samples, labels):
    A = np.zeros((samples.shape[1], samples.shape[1]))
    for cls in labels.unique():
        A += scatter_matrix(samples[labels == cls])
    return A


def find_mahl_sqr_dist(centers, samples, covr):
    res = pd.DataFrame(index=samples.index, columns=centers.index)
    for i in centers.index:
        for j in samples.index:
            res[i][j] = mahalanobis(centers.loc[i], samples.loc[j], np.linalg.inv(covr)) ** 2
    return res


def get_def_coef(lda, features):
    return pd.DataFrame(
        np.vstack([lda.intercept_, lda.coef_.T]),
        index=["Const"] + features,
        columns=lda.classes_
    )


def LDA_predict(lda, x):
    return pd.DataFrame(
        lda.predict(x),
        columns=["Class"],
        index=x.index
    )


def LDA_predict_probab(lda, x):
    return pd.DataFrame(
        lda.predict_proba(x),
        columns=lda.classes_,
        index=x.index
    )


def wilks_lambda(samples, labels):
    if isinstance(samples, pd.Series):
        samples = samples.to_frame()
    # определитель матрицы рассеивания
    dT = np.linalg.det(scatter_matrix(samples))
    # определитель классовой матрицы рассеивания
    dE = np.linalg.det(classes_scatter_matrix(samples, labels))
    return dE / dT


def f_p_value(lmbd, n_obj, n_sign, n_cls):
    num = (1-lmbd)*(n_obj - n_cls - n_sign)
    den = lmbd * (n_cls - 1)
    f_value = num / den
    p = f.sf(f_value, n_cls-1, n_obj-n_cls-n_sign)
    return f_value, p


def forward(samples, labels, f_in = 1e-4):
    st_columns = ["Wilk's lmbd", "Partial lmbd", "F to enter", "P value"]
    n_cls = labels.unique().size
    n_obj = samples.shape[0]
    # хранение пременных вне и в модели(е)
    out = {0: pd.DataFrame(columns=st_columns, index=samples.columns, dtype=float)}
    into = {0: pd.DataFrame(columns=st_columns, dtype=float)}
    step = 0

    while True:
        model_lmbd = wilks_lambda(samples[into[step].index], labels)
        # расчёт характеристик элементов вне модели
        for el in out[step].index:
            lmbda = wilks_lambda(samples[into[step].index.tolist() + [el]], labels)
            partial_lmbd = lmbda / model_lmbd
            f_lmbd, p_value = f_p_value(partial_lmbd, n_obj, into[step].index.size, n_cls)
            out[step].loc[el] = lmbda, partial_lmbd, f_lmbd, p_value
        # расчёт характеристик элементов в моделе
        for el in into[step].index:
            lmbda = wilks_lambda(samples[into[step].index.drop(el)], labels)
            partial_lmbd = model_lmbd / lmbda
            f_lmbd, p_value = f_p_value(partial_lmbd, n_obj, into[step].index.size-1, n_cls)
            into[step].loc[el] = lmbda, partial_lmbd, f_lmbd, p_value

        if out[step].index.size == 0 or out[step]["F to enter"].max() < f_in:
            break
        # добавление нового элемента
        el_to_enter = out[step]["F to enter"].idxmax()
        into[step+1] = into[step].append(out[step].loc[el_to_enter])
        out[step+1] = out[step].drop(index=el_to_enter)

        step += 1
    return into, out


def backward(samples, labels, f_r = 1e+4):
    st_columns = ["Wilk's lmbd", "Partial lmbd", "F to remove", "P value"]
    n_cls = labels.unique().size
    n_obj = samples.shape[0]
    # хранение пременных вне и в модели(е)
    into = {0: pd.DataFrame(columns=st_columns, index=samples.columns, dtype=float)}
    out = {0: pd.DataFrame(columns=st_columns, dtype=float)}
    step = 0

    while True:
        model_lmbd = wilks_lambda(samples[into[step].index], labels)
        # расчёт характеристик элементов вне модели
        for el in out[step].index:
            lmbda = wilks_lambda(samples[into[step].index.tolist() + [el]], labels)
            partial_lmbd = lmbda / model_lmbd
            f_lmbd, p_value = f_p_value(partial_lmbd, n_obj, into[step].index.size, n_cls)
            out[step].loc[el] = lmbda, partial_lmbd, f_lmbd, p_value
        # расчёт характеристик элементов в моделе
        for el in into[step].index:
            lmbda = wilks_lambda(samples[into[step].index.drop(el)], labels)
            partial_lmbd = model_lmbd / lmbda
            f_lmbd, p_value = f_p_value(partial_lmbd, n_obj, into[step].index.size-1, n_cls)
            into[step].loc[el] = lmbda, partial_lmbd, f_lmbd, p_value

        if into[step].index.size == 0 or into[step]["F to remove"].min() > f_r:
            break
        # удаление элемента
        el_to_remove = into[step]["F to remove"].idxmin()
        out[step+1] = out[step].append(into[step].loc[el_to_remove])
        into[step+1] = into[step].drop(index=el_to_remove)

        step += 1
    return into, out


FEATURES = ["X1", "X2", "X3", "X4", "X5", "X6", "X7", "X8", "X9", "X10"]

TRAIN_SAMPLES = {
    1: [59, 61, 65],
    2: [0, 1, 4, 8, 9, 10, 11, 12, 13, 15],
    3: [2, 3, 5, 6, 7, 14, 16, 18, 20, 25, 26],
    4: [79, 80],
    5: [70, 74],
}

data = pd.read_excel(r'C:\Users\linke\Desktop\Data.xlsx', usecols=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
data = data.loc[range(0, 85)]
print("Данные")
print(data.head())
data_to_excel = data[FEATURES]

train_data = get_train_data(data, FEATURES, TRAIN_SAMPLES)
print("Обучающая выборка")
print(train_data)
data_to_excel["Train sample"] = train_data.Class

cov = pd.DataFrame(
    classes_scatter_matrix(train_data[FEATURES], train_data.Class) / (train_data.shape[0] - train_data.Class.unique()
                                                                      .size),
    index=FEATURES,
    columns=FEATURES
)
print("Ковариационная матрица")
print(cov)

lda = LinearDiscriminantAnalysis().fit(train_data[FEATURES], train_data.Class)
means = pd.DataFrame(lda.means_, index=lda.classes_, columns=FEATURES)
print("Средние значения")
print(means)

cen_dis = find_mahl_sqr_dist(means, means, cov)
print("Расстояние Махланобиса (обучающая выборка)")
print(cen_dis)

df_coef = get_def_coef(lda, FEATURES)
print("Функции Фишера")
print(df_coef)

print("Pi: ", lda.priors_)

lda_predict = LDA_predict(lda, data[FEATURES])
print("Распределение по классам")
print(lda_predict)
data_to_excel["Result Lda"] = lda_predict

samp_dist = find_mahl_sqr_dist(means, data[FEATURES], cov)
print("Расстояние Махланобиса")
print(samp_dist)

lda_post_prob = LDA_predict_probab(lda, data[FEATURES])
print("Probabilities")
print(lda_post_prob)

into, out = forward(train_data[FEATURES], train_data.Class)
print("Forward stepwise")
for i, tab in into.items():
    print("Step: ", i)
    print(tab, end="\n\n")

forw_stepwise = into[len(into) - 4].index.tolist()
print(forw_stepwise)
forw_stepwise_lda = LinearDiscriminantAnalysis().fit(train_data[forw_stepwise], train_data.Class)
forw_stepwise_coef = get_def_coef(forw_stepwise_lda, forw_stepwise)
print("Функции Фишера")
print(forw_stepwise_coef)
print("Pi: ", forw_stepwise_lda.priors_)
forw_stepwise_pred = LDA_predict(forw_stepwise_lda, data[forw_stepwise])
print("Распределение")
print(forw_stepwise_pred.head())
data_to_excel["Result forward"] = forw_stepwise_pred

into, out = backward(train_data[FEATURES], train_data.Class)
print("Backward stepwise")
for i, tab in into.items():
    print("Step: ", i)
    print(tab, end="\n\n")

back_stepwise = into[len(into) - 7].index.tolist()
print(back_stepwise)
back_stepwise_lda = LinearDiscriminantAnalysis().fit(train_data[back_stepwise], train_data.Class)
back_stepwise_coef = get_def_coef(back_stepwise_lda, back_stepwise)
print("Функции Фишера")
print(back_stepwise_coef)
print("Pi: ", back_stepwise_lda.priors_)
back_stepwise_pred = LDA_predict(back_stepwise_lda, data[back_stepwise])
print("Распределение")
print(back_stepwise_pred.head())
data_to_excel["Result backward"] = back_stepwise_pred
data_to_excel.to_excel(r'C:\Users\linke\Desktop\Data_new.xlsx')
